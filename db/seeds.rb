# All the record creation needed to seed the database with its default values.
# run: rails db:seed
['admin', 'contributor', 'moderator', 'supervisor', 'suscribed', 'registred'].each do |role|
  Role.find_or_create_by({name: role})
end
