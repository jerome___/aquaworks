class ChangeColumnPublishedAtFromArticles < ActiveRecord::Migration[5.2]
  def change
    change_column :articles, :published_at, :timestamp
  end
end
