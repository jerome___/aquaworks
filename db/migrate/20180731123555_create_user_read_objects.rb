class CreateUserReadObjects < ActiveRecord::Migration[5.2]
  def change
    create_table :user_read_objects do |t|
      t.references :user, foreign_key: true
      t.integer :object_id
      t.string :object_name
      t.boolean :read

      t.timestamps
    end
  end
end
