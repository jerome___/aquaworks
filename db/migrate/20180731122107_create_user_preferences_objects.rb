class CreateUserPreferencesObjects < ActiveRecord::Migration[5.2]
  def change
    create_table :user_preferences_objects do |t|
      t.references :user, foreign_key: true
      t.string :created_from
      t.string :created_to
      t.string :updated_from
      t.string :updated_to
      t.string :published_from
      t.string :pusblished_to
      t.string :object
      t.integer :object_id
      t.boolean :own

      t.timestamps
    end
    add_index :user_preferences_objects, [:user_id, :object, :object_id, :own], unique: true, name: "constraint_indexed_user_preferencess_objects"
  end
end
