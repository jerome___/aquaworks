class AddColumnRstrictedPrivateToArticles < ActiveRecord::Migration[5.2]
  def change
    add_column :articles, :private, :bool
    add_column :articles, :restricted, :bool
  end
end
