class AddColumnToSubjects < ActiveRecord::Migration[5.2]
  def change
    add_column :subjects, :published_at, :timestamp
    add_column :subjects, :restricted, :bool
    add_column :subjects, :private, :bool
  end
end
