class SubjectsController < ApplicationController
  authorize_resource
  before_action :set_subject, only: [:show, :edit, :update, :destroy]
  before_action :login_required, only: [:new, :create, :edit, :update, :destroy]
  after_action :flash_discard, only: [:show, :index]

  def index                   # GET /subjects or /subjects.json
    if params.has_key?(:category_id)
      @category = Category.find(params[:category_id].to_i)
      if helpers.administrator? 
        @subjects = Subject .where(category_id: params[:category_id])
                            .order(id: :asc)
      else
        @subjects = Subject.where(category_id: params[:category_id])
          .publishable_only.order(id: :asc)
      end
      @mode = "index_category"
    else
      if helpers.administrator?
        @subjects = Subject.order(id: :asc)
      else
        @subjects = Subject.order(id: :asc).publishable_only
      end
      @mode = "index"
    end
    @subjects = @subjects.page(params[:page]).per(5)
  end

  def show                    # GET /subjects/1 or /subjects/1.json
  end

  def publication   # POST  :online == "yes" ? DateTime.now : null
    @subject = Subject.find(params[:id])
    @title = params[:title]
    if params.has_key?(:online)
      @subject.published_at = params[:online] == "yes" ? DateTime.current : nil
    end
    respond_to do |f|
      if @subject.update(id: params[:id], published_at: @subject.published_at)
        notification = "Subject \"#{@subject.title}\" has been "
        notification += @subject.published_at ? "published" : "unpublished"
        flash.now[:success] = notification
        f.html { redirect_to controller: params[:k], action: params[:a] }
        f.js { render layout: false }
      else
        flash.now[:error] = "Publication state of Subject \"#{@subject.title}\" update failed"
        f.html { redirect_to controller: params[:k], action: params[:a] }
      end
    end
  end

  def new                     # GET /subjects/new
    @title = "Create"
    @cat_disabled = false
    @parent_disabled = false
    @subject = Subject.new
    if !params[:parent_id].nil?
      @subject.parent_id = params[:parent_id]
      @cat_disabled = true
      @parent_disabled = true
    end
    if !params[:category_id].nil?
      @subject.category_id = params[:category_id]
    end
    respond_to do |f|
      f.html
      f.js { render layout: false }
    end
  end

  def create                  # POST /subjects or /subjects.json
    @subject = Subject.new(subject_params)
    if params.has_key?(:create_and_publish)
      @subject.published_at = DateTime.current
    end
    if params.has_key?(:parent_id)
      parent = Subject.find(params[:parent_id])
      parent.add_child @subject
    end
    respond_to do |f|
      if @subject.save
        flash[:success] = "Subject \"#{@subject.title}\" was successfully created."
        f.html { redirect_to @subject }
        f.json { render :show, status: :created, location: @subject }
        f.js { render layout: false }
      else
        flash[:error] = "Failed to save subject \"#{@subject.title}\""
        f.html { render :new }
        f.json { render json: @subject.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit                    # GET /subjects/1/edit
    @title = "Edit"
    @cat_disabled = false
    @parent_disabled = false
    respond_to do |f|
      f.html
      f.js { render layout: false }
    end
  end

  def update                  # PATCH/PUT /subjects/1 or /subjects/1.json
    respond_to do |format|
      if @subject.update(subject_params)
        flash[:success] = "Subject \"#{@subject.title}\" was successfully updated."
        format.html { redirect_to @subject }
        format.json { render :show, status: :ok, location: @subject }
        format.js
      else
        flash[:error] = "Failed to update subject \"#{@subject.title}\""
        format.html { render :edit }
        format.json { render json: @subject.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy                 # DELETE /subjects/1 or /subjects/1.json
    @subject.destroy
    respond_to do |format|
      flash[:success] = "Subject \"#{@subject.title}\" was successfully destroyed."
      format.html { redirect_to subjects_url }
      format.json { head :no_content }
      format.js { render layout: false }
    end
  end

  private
    def set_subject
      @subject = Subject.find(params[:id])
      #if !helpers.administrator? && !helpers.can_publish_subjects?(@subject)
      #  @subject = nil
      #end
    end

    def subject_params
      params.require(:subject).permit(:title, :description, :logo, 
                                      :published_at, :restricted, :private,
                                      :parent_id, :category_id,
                                      articles_attributes: [:id, :title, :description, 
                                                            :content, :_destroy])
    end

    def login_required
      if !user_signed_in?
        flash[:alert] = "User has to be logged in for this action."
        redirect_to subjects_url
      end
    end

    def flash_discard
      flash.discard if request.xhr?
    end
end
