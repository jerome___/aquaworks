class CategoriesController < ApplicationController
  authorize_resource
  before_action :login_required, only: [:new, :create, :edit, :update, :destroy]
  before_action :set_category, only: [:show, :edit, :update, :destroy]

 # add_breadcrumb("home", "/")

  def index                   # GET /categories or /categories.json
   # add_breadcrumb "Catégories", categories_path
    @categories = Category.with_attached_image
  end

  def show                    # GET /categories/1 or  /categories/1.json
    #add_breadcrumb @category.name, category_path(params[:id])
    data = []
    @subjects = @category.subjects.find_all_by_generation(0)
    @subjects.each do |subject|
      list_subjects(subject, '#', data)
    end
    respond_to do |f|
      f.json { render json: data }
      f.html
    end
  end

  def show_object
    if params[:obj] == 'subject'
      @subject = Subject.find(params[:oid])
      render partial: "show/content_subject", object: @subject
    elsif params[:obj] == 'article'
      @article = Article.find(params[:oid])
      render partial: "show/content_article", object: @article
    end
  end
  
  def new                     # GET /categories/new
    @category = Category.new
    @name = "Create"
    respond_to do |f|
      f.html
      f.js { render layout: false }
    end
  end

  def edit                    # GET /categories/1/edit
  end

  def create                  # POST /categories or /categories.json
    @category = Category.new(category_params)
    respond_to do |format|
      if @category.save
        format.html { redirect_to @category, notice: 'Category was successfully created.' }
        format.json { render :show, status: :created, location: @category }
      else
        format.html { render :new }
        format.json { render json: @category.errors, status: :unprocessable_entity }
      end
    end
  end

  def update                  # PATCH/PUT /categories/1 or /categories/1.json
    respond_to do |format|
      if @category.update(category_params)
        format.html { redirect_to @category, notice: 'Category was successfully updated.' }
        format.json { render :show, status: :ok, location: @category }
      else
        format.html { render :edit }
        format.json { render json: @category.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy                 # DELETE /categories/1.json or /categories/1
    @category.destroy
    respond_to do |format|
      format.html { redirect_to categories_url, notice: 'Category was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

    def list_subjects( sub, pid, data )
      id = sub.id.to_s + "S"
      data << { :id => id, :parent => pid, :text => sub.title.to_s, :state => { :opened => true }, :type => 'subject', :a_attr => { :style => 'color: rgb(0,99,73);' } }
      sub.articles.each do |article|
        data << { :id => article.id, :parent => id, :text => article.title.to_s, :state => { :opened => true }, :type => 'article' }
      end
      sub.children.each do |child|
        list_subjects(child, id, data)
      end
    end
  
    def set_category
      @category = Category.find(params[:id])
    end

    def category_params
      params.require(:category).permit(:name, :description, :image,
                                       subjects_attributes: [:id, :title, 
                                                             :description, 
                                                             :_destroy])
    end

    def login_required
      if !user_signed_in?
        redirect_to categories_url, alert: 'You have to login for this action.'
      end
    end
end
