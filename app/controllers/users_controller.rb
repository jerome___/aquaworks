class UsersController < ApplicationController

  before_action :set_params, only: [:show, :destroy, :destroy_role, :create_role, :update]
  before_action :login_required, only: [:destroy, :show, :update]
  before_action :admin_role_required, only: [:destroy, :new_role, :destroy_role, :create_role]

  def show
  end

  # POST | AJAX return if username exist and if account confirmed
  def exist_confirmed
    @user = User.where(username: params[:username]).first
    if @user
      user_ = @user.as_json
      user_.store("confirmed", @user.confirmed?)
    respond_to do |f|
      f.json { render json: user_.to_json }
    end
    end
  end

  # POST | AJAX call for users/send_confirmation?:username
  # to send back confirmation to username existing user if exist
  def send_confirmation_again
    username = params[:username]
    puts("=============================================")
    puts("username = #{username}")
    puts("=============================================")
    @user = User.where(username: username)
    respond_to do |f|
      if @user.count == 1
        @user.first.send_confirmation_instructions
        flash[:succes] = "user #{username} should receipt confirmation email soon."
        f.js { render layout: false }
      else
        flash[:error] = "No user #{username}"
      end
    end
  end

  # GET | admin only can create a new role for a user from his profile view (users tab)
  # this will open and populate the modal window to get "new role" form view.
  def new_role
    @user = User.find(params[:user_id])
    respond_to do |f|
      f.js { render layout: false }
    end
  end

  # POST | AJAX call to get "resource_type_name" selection 
  # and return list of resource_ids/names child related
  def resources_for_type
    @resources = resource_contents(params[:resource_type_name])
    respond_to do |f|
      f.js { render layout: false }
    end
  end

  # DELETE | admin can delete a user role from his profil view (users tab)
  def destroy_role
    @user.remove_only_role_relation params[:role_id]
    respond_to do |f|
      f.js {render layout: false }
    end
  end

  # POST | admin can create a user role from his profil view (users tab)
  # and from the modal window "New role" form called from #new_role
  def create_role
    @role = Role.new(role_params)
    if !@role.resource_type.nil?
      resource_object = !@role.resource_id.nil? ? 
        resource_content(@role.resource_type, @role.resource_id) :
        @role.resource_type.constantize
      @user.add_role @role.name, resource_object
    else
      @user.add_role @role.name
    end
    respond_to do |f|
      if @user.save
        flash[:success] = "succefully updated"
        f.js { render layout: false }
      else
        flash[:error] = "failed to update"
      end
    end
  end

  def update
    if @user.update(user_params)
      flash[:success] = "User successfully updated"
      redirect_to @user
    else
      flash[:error] = "User failed to update"
      render :edit
    end
  end

  # DELETE | a user can delete his own account.
  # Admin users can delete any accounts
  def destroy
    username = @user.username
    @user.destroy
    respond_to do |format|
      format.js { render layout: false }
      format.html { 
        redirect_to root_url, notice: "User #{username} has been successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private

  def user_params
    params.require(:user).permit(:id, :username, :familly_name, :second_name,
                                 roles_attributes: [:name, :resource_type, :resource_name, :id], 
                                 articles_attributes: [ :title, :description, 
                                                        :content, :logo ])
  end

  def role_params
    params.require(:roles).permit(:name, :resource_type, :resource_id )
  end

  def set_params
    @user = User.find(params[:id])
  end

  def login_required
    if !user_signed_in?
      redirect_to articles_url, alert: "You have to login first for this action."
    end
  end

  def admin_role_required
    if !current_user.has_role? :admin
      redirect_to user_url, alert: "You have to be admin for execute this action."
    end
  end

  def resource_content(type, id)
    case type
    when "Category"
      return Category.find(id)
    when "Subject"
      return Subject.find(id)
    when "Article"
      return Article.find(id)
    end
  end

  def resource_contents(type)
    case type
    when "Category"
      return Category.all.collect{ |c| [ c.id, c.name ] }
    when "Subject"
      return Subject.all.collect{ |s| [ s.id, s.title ] }
    when "Article"
      return Article.all.collect{ |a| [ a.id, a.title ] }
    end
  end

end
