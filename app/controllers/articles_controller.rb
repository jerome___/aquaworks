class ArticlesController < ApplicationController
  authorize_resource
  before_action :set_article, only: [:show, :edit, :update, :destroy]
  before_action :login_required, only: [:new, :create, :edit, :update, :destroy]

  def fresh_index
    if !user_signed_in?
      if !params.has_key?(:date)
        @articles = Article.where("updated_at > ?", 1.week.ago)
                           .order(id: :asc)
      end
    end
    @articles = Article.where("updated_at > ?", 1.week.ago)
                       .publishable_subjects_only
                       .published_only
                       .order(id: :asc)
  end

  def index
    if helpers.administrator?
      @articles = Article.all.order(id: :asc)
      #@article = Article.new
    else
      @articles = Article.where.not(published_at: nil)
                         .order(id: :asc)
                         .publishable_subjects_only
    end
    if params[:tag]
      @articles = @articles.tagged_with(params[:tag])
    end
    @articles = @articles.page(params[:page]).per(10)
    if @articles.count == 0
      @articles = @articles.page(params[:page].to_i - 1).per(10)
    end
    respond_to do |format|
      format.js { render layout: false }
      format.html 
      format.json 
    end
  end

  def publication   # POST  :online == "yes" ? DateTime.now : null
    @article = Article.find(params[:id])
    @title = params[:title]
    if params.has_key?(:online)
    @article.published_at = params[:online] == "yes" ? DateTime.current : nil
    end
    gon.data = @article
    respond_to do |f|
      if @article.update(id:            params[:id], 
                         published_at:  @article.published_at)
        f.js { render layout: false }
        f.html { redirect_to controller: params[:k], 
                             action:     params[:a],
                             notice:     'Article has been pusblished' }
      else
        f.html { redirect_to controller: params[:k], 
                             action:     params[:a],
                             status:     'Article failed to be pusblished' }
      end
    end
  end

  def show
    if !@article.present?
      redirect_to articles_url, 
                  notice: "Article doesn't exist or is not published"
    end
  end

  def new
    @subject_disabled = false
    @title = "Create"
    @article = Article.new
    @article.user_id = current_user.id
    if !params[:subject_id].nil?
      @article.subject_id = params[:subject_id]
      @subject_disabled = true
      @title += " child"
    end
  end

  def edit
    @subject_disabled = false
    @title = "Edit"
    respond_to do |format|
      format.js { render layout: false }
      format.html
    end
  end

  def create
    @article = Article.new(article_params)
    @article.user_id = current_user.id
    if params.has_key?(:create_and_publish)
      @article.published_at = DateTime.current
    end
    respond_to do |format|
      if @result = @article.save
        format.js { render layout: false }
        format.html { 
          redirect_to @article, 
                      notice: 'Article was successfully created.' }
        format.json { 
          render :show, 
                 status: :created, 
                 location: @article }
      else
        gon.data = @article
        format.js { render layout: false }
        format.html { render :new }
        format.json { 
          render json: @article.errors, 
                 status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @article.update(article_params)
        format.js { render layout: false }
        format.html { 
          redirect_to @article, 
                       notice: 'Article was successfully updated.' }
        format.json 
      else
        gon.data = @article
        format.html { render :edit }
        format.json { 
          render json: @article.errors, 
                 status: :unprocessable_entity }
      end
    end
  end

  def destroy
    respond_to do |format|
      if @article.destroy
        format.js { render layout: false }
        format.html { 
          redirect_to articles_url, 
                      notice: 'Article was successfully destroyed.' }
        format.json { head :no_content }
      else
        gon.data = @article
      end
    end
  end

  private
    def set_article
      @article = Article.find(params[:id])
      if !helpers.administrator? && 
         !helpers.can_publish_article?(@article)
        @article = nil
      end
    end

    def article_params
      params.require(:article).permit(:title, :description, :content, 
                                      :logo, :subject_id, :user_id,
                                      :published_at, :restricted, :private,
                                      :interest_list, :_destroy ,
                                      interests_attributes: [:id, :name, 
                                                             :taggings_count, 
                                                             :_destroy])
    end

    def login_required
      if !user_signed_in?
        redirect_to articles_url, alert: "You have to login first for this action."
      end
    end

end
