class NavigationsController < ApplicationController
  def index
  end

  def show_object
    if params[:obj] == 'category'
      @category = Category.find(params[:oid])
      render partial: "navigations/show/content_category", object: @category
    elsif params[:obj] == 'subject'
      @subject = Subject.find(params[:oid])
      render partial: "navigations/show/content_subject", object: @subject
    elsif params[:obj] == 'article'
      @article = Article.find(params[:oid])
      render partial: "navigations/show/content_article", object: @article
    end
  end
  
  def dated
  end

  def suscribed
  end

  def unread
  end

  def owner
  end

  def nav
    @categories = Category.all()
    data = list_categories(@categories)
    render json: data
  end

  private

  def list_categories(categories)
    data = []
    categories.each do |category|
      cid = category.id.to_s + "C"
      data << { id: cid, parent: "#", text: category.name.to_s, 
                state: { opened: true }, type: "category", 
                a_attr: { style: 'color: rgb(0,99,73);'} }
      list_subjects(category.subjects, cid, data)
    end 
    puts data
    return data
  end

  def list_subjects(subjects, pid, data)
    if (user_signed_in?) && (current_user.has_role? :admin)
      view_subjects = subjects.all
    else
      view_subjects = subjects.publishable_only
    end
    view_subjects.each do |subject|
      sid = subject.id.to_s + "S"
      parent_id = subject.parent_id.nil? ? pid 
                : subject.parent_id.to_s + "S"
      data << { id: sid, parent: parent_id, text: subject.title.to_s, 
                state: { opened: true }, type: "subject", 
                a_attr: { style: 'color: rgb(0,D0,23);'} }
      list_articles(subject.articles, sid, data)
    end
  end

  def list_articles(articles, pid, data)
    articles.each do |article|
      aid = article.id.to_s + "A"
      data << { id: aid, parent: pid, text: article.title.to_s, 
                state: { opened: true }, type: "article", 
                a_attr: { style: 'color: rgb(169,76,23);'} }
    end
  end

end
