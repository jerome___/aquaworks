module CategoriesHelper

  def subjects_count(category)
    category.subjects.count
  end

  def total_articles(category)
    Subject.where(category_id: category.id).map { 
      |s| s.articles.count }.sum
  end

  def publishable_subjects_count(category)
    Subject.where(category_id: category.id).publishable_only.count
  end

  def publishable_articles_count(category)
    Subject.where(category_id: category.id).publishable_only.map {
      |s| s.articles.map { |a|
        a.published_at.nil? ? 0 : 1 }.sum 
    }.sum
  end

  def children_subjects_count(c)
    c.subjects.descendants.map { |cc|
      cc.subjects.count }.sum
  end

  def subjects_total(c)
    c.subjects.count + children_subjects_count(c)
  end

  def published_own_subjects_count(c)
    c.subjects.map { |s| s.published_at.nil? ? 0 : 1 }.sum
  end

  # sum of subjects published
  def published_children_subjects_count(c)
    blacklisted = []
    somme = 0
    c.subjects.each { |s|
      s.descendants.each { |cc|
        if cc.published_at.nil?
          blacklisted += cc.self_and_descendants.ids
        end }
      blacklisted = blacklisted.uniq
      somme += s.descendants.map { |cc|
        if !blacklisted.include?(cc.id) 
          cc.published_at.nil? ? 0 : 1
        else
          0
        end
      }.sum
    }
    return somme
  end

  def c_published_subjects_total(c)
    published_own_subjects_count(c) + published_children_subjects_count(c)
  end
end
