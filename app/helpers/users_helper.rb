module UsersHelper

  def resource_contents(type)
    case type
    when "Category"
      return Category.all.collect{ |c| { name: c.name, id: c.id } }
    when "Subject"
      return Subject.all.collect{ |s| { name: s.title, id: s.id } }
    when "Article"
      return Articles.all.collect{ -a| { name: a.title, id: a.id } }
    end
  end

  def resource_content(type, id)
    case type
    when "Category"
      return Category.find(id)
    when "Subject"
      return SUbject.find(id)
    when "Article"
      return Article.find(id)
    end
  end

  def resource_cname(type, id)
    case type
    when "Category"
      return Category.find(id).name
    when "Subject"
      return Subject.find(id).title
    when "Article"
      return Article.find(id).title
    end
  end

end
