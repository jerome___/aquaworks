module WelcomeHelper

  def category_subjects_total(cat)
    cat.subjects.count
  end

  def category_articles_total(cat)
    cat.subjects.map { |s| s.articles.count }.sum
  end

  def category_publishable_subjects_total(cat)
    blacklisted = unpublishable_subjects_from_category(cat)
    cat.subjects.count - blacklisted.count
  end

  def category_publishable_articles_total(cat)
    subjects = Subject.where(category_id: cat.id).publishable_only
    subjects.map { |s|
      s.articles.map { |a|
        a.published_at.nil? ? 0 : 1 }.sum }.sum
  end

  def unpublishable_subjects_from_category(cat)
    blacklisted = []
    cat.subjects.each do |subject|
      subject.self_and_descendants.each { |s|
        if s.published_at.nil?
          blacklisted += s.self_and_descendants.ids
        end }
    end
    blacklisted.uniq
  end

  def unpublishable_subjects_all()
    blacklisted = []
    subjects = Subject.all
    subjects.each do |subject|
      subject.self_and_descendants.each { |s|
        if s.published_at.nil?
          blacklisted += s.self_and_descendants.ids
        end }
    end
    blacklisted.uniq
  end

end
