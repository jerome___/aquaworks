module ArticlesHelper

  def get_article_color(a)
    a.published_at.nil? ? 
      "red" : 
      !can_publish_subjects?(a.subject) ? 
        "orange" : 
        "green"
  end

  def selected_articles(articles)
    articles.published_only.reject{ |a| !can_publish_subjects?(a.subject) }
  end
end
