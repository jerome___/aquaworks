module SubjectsHelper

  def published_own_articles_count(s)
    s.articles.map { |a| a.published_at.nil? ? 0 : 1 }.sum
  end

  def published_children_articles_count(s)  # sum of articles published
    blacklisted = []                        # of subjects child published
    s.descendants.each { |sc|               # for all parents published subjects
      if sc.published_at.nil?
        blacklisted += sc.self_and_descendants.ids
      end }
    blacklisted = blacklisted.uniq
    s.descendants.map { |sc|
      if !blacklisted.include?(sc.id) 
        sc.articles.map{ |sca| 
          sca.published_at.nil? ? 0 : 1}.sum
      else
        0
      end
    }.sum
  end

  def published_subjects_children_articles_count(subjects)
    subjects.empty? ? 0 : only_first_level(subjects).map { |s| published_articles_total(s)}.sum
  end

  def children_articles_count(s)
    s.descendants.map { |sc|
      sc.articles.count }.sum
  end

  def published_articles_total(s)
    published_own_articles_count(s) + published_children_articles_count(s)
  end

  # sum of published articles
  def subjects_published_articles_total(subjects)
    subjects.map { |s|
      s.published_at.nil? ? 0 : published_articles_total(s) }.sum
  end

  def articles_total(s)
    s.articles.count + children_articles_count(s)
  end

  def subjects_articles_total(subjects)
    only_first_level(subjects).map { |s|
      articles_total(s) }.sum
  end

  def published_subject_children_count(s)
    blacklisted = []
    s.self_and_descendants.each do |sc|
      if sc.published_at.nil?
        blacklisted += sc.self_and_descendants.ids
      end
    end
    blacklisted = blacklisted.uniq
    s.self_and_descendants.map { |sc|
      if !blacklisted.include? sc.id
        sc.published_at.nil? ? 0 : 1 
      else
        0
      end
    }.sum
  end
  
  def subjects_published_subjects_children_count(subjects)
    return 0 if subjects.count == 0
    only_first_level(subjects).map { |s| 
      published_subject_children_count(s) }.sum
  end

  def only_first_level(subjects)
    if !subjects.empty?
      puts("Subjects: #{subjects}")
      remember = subjects.first.parent.present? ? 
        subjects.first.parent.id : nil
      subjects.reject { |s| 
        s.parent_id != remember }
    end
  end

  def published_subjects_total(s)
    s.self_and_descendants.map { |ssc|
      ssc.published_at.nil? ? 0 : 1 }.sum
  end

  def subjects_published_subjects_total(subjects)
    subjects.map { |s|
      published_subjects_total(s) }.sum
  end

  def category_published_subjects_total(category)
    category.subjects.map { |subject|
      published_subjects_total(subject) }.sum
  end

  def subjects_children_count(s, sum = 0)
    s.children.each do |sc|
      sum += 1
      subjects_children_count(sc, sum)
    end
  end

  def reject_if_unpublished_self_or_any_parent(s)
    if s.published_at.nil?
      return true
    elsif s.parent
      reject_if_unpublished_self_or_any_parent(s.parent)
    end
  end

  def reject_if_unpublished(subjects)
    subjects.reject { |s| 
      reject_if_unpublished_self_or_any_parent(s)
    }
  end

  def only_publishable_descendants(subject)
    subject.descendants.reject { |d| 
      reject_if_unpublished_self_or_any_parent(d)
    }
  end

  def subject_color(subject)
    subject.published_at.nil? ? "red" :
      subject_parents_publishable?(subject) ? 
        "green" : 
        "orange"
  end

  def subject_parents_publishable?(subject)
    subject.ancestors.each { |sa|
      if sa.published_at.nil?
        return false
      end }
    return true
  end

end
