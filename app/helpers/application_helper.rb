module ApplicationHelper

  def administrator?
    if user_signed_in?
      return current_user.has_role? :admin
    else
      return false
    end
  end

  def can_publish_subjects?(s)
    if s.published_at.nil? || !s.present? 
      return false
    elsif s.parent.present?
      return can_publish_subjects?(s.parent)
    else
      return true
    end
  end

  def can_publish_article?(a)
    return false if a.published_at.nil?
    can_publish_subjects?(a.subject)
  end

  def get_articles_from(user)

  end

  def get_created_articles_from(date, 
                                comparator, 
                                articles,
                                user = _all_,
                                user_prefs = {})

  end

  def get_updated_articles_from(date, 
                                comparator, 
                                articles,
                                user = _all_,
                                user_prefs = {})

  end

  def user_topX_list()
    Article.all.published_only
  end

  def flash_class(level)
    case level
        when "notice" then "alert alert-info"
        when "success" then "alert alert-success"
        when "error" then "alert alert-error"
        when "alert" then "alert alert-warning"
    end
  end

end
