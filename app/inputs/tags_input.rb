class TagsInput < SimpleForm::Inputs::StringInput
  def input(wrapper_options = nil)
    data = object.send(attribute_name).join(", ")
    input_html_options[:value] = data
    out = ActiveSupport::SafeBuffer.new
    out << @builder.text_field(attribute_name, input_html_options)
  end
end
