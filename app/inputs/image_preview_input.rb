class ImagePreviewInput < SimpleForm::Inputs::FileInput
  def input(wrapper_options = nil)
    out = ActiveSupport::SafeBuffer.new
    # check if there's an uploaded file (eg: edit mode or form not saved)
    if object.send("#{attribute_name}_attachment")
      out << template.image_tag(object.send(attribute_name))
    end
    out << @builder.hidden_field("#{attribute_name}_attachment").html_safe
    out << @builder.file_field(attribute_name, input_html_options)
  end
end
