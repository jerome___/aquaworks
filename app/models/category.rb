class Category < ApplicationRecord
  resourcify
  has_many :subjects,  dependent: :destroy
  accepts_nested_attributes_for :subjects, 
      reject_if: proc { |attributes| attributes[:title].blank? }, 
      allow_destroy: true
  has_one_attached :image

  validates :name,  presence: true,
                    length: { maximum: 16 },
                    uniqueness: true
  validates :description, length: { maximum: 256 }
end
