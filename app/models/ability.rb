class Ability
  include CanCan::Ability

  def initialize(user)
    alias_action :create, :read, :update, :destroy, to: :crud
    # Define abilities for the passed in user here. For example:
    alias_action :read, :update, to: :moderate
    #
    if user.nil?
      user = User.new # guest user (not logged in)
      can :fresh_index, Article, published_only: true
      can :read, [ Article, Subject ], private: false, 
                                       restricted: false, 
                                       published_only: true
      can :read, Category
    end
    if user.present?
      can :access, :ckeditor
      #can [:read, :create, :destroy], Ckeditor::Picture
      #can [:read, :create, :destroy], Ckeditor::AttachmentFile

      if user.has_role? :admin
        can :manage, :all
      elsif user.has_role? :moderator
        can :moderate, Article
        can :read, :all
      elsif user.has_role? :contributor
        can :crud, Article, user_id: user.id
      end
    end

    # Synoptic:
    # <ability> <action>, <reource>, [<filters>, ...]
    # <ability>  : `can`/'cannot' ability action
    # <action>   : action(s) type(s) (or alias group of actions list)
    #                 actions     -> :read, :create, :update and :destroy
    #                 alias group => :manage
    # <resource> : can be a Model class or :all (for apply on all resources)
    #
    # <filters>  : optional hash of conditions filters/scopes for Model object
  end
end
