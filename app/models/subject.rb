class Subject < ApplicationRecord
  resourcify
  has_closure_tree
  paginates_per 10
  has_many :articles, inverse_of: :subject, dependent: :destroy
  belongs_to :category
  accepts_nested_attributes_for :articles, 
    reject_if: proc { |attributes| attributes[:title].blank? }, 
    allow_destroy: true
  has_one_attached :logo

  validates :title, presence: true,
                    length: { maximum: 16 },
                    uniqueness: true
  validates :description, length: { maximum: 512 }
  validates_associated :category
  validate :logo_check

  scope :blacklisted_ids, -> (ids) { where.not(id: ids) }

  def self.publishable_only
    unpublished_and_child = []
    Subject.where(published_at: nil).each { |a|
      unpublished_and_child += a.self_and_descendants.ids }
    blacklisted_ids(unpublished_and_child.uniq) 
  end

  def little_thumb
    return self.logo.variant(resize: "42x42!").processed
  end

  def medium_thumb
    return self.logo.variant(resize: "150x150!").processed
  end

  def big_thumb
    return self.logo.variant(resize: "300x300!").processed
  end

  private

  def logo_check
    if logo.attached? == false
      errors.add(:logo, "is missing")
    end
    if !logo.content_type.in?(%("image/jpeg image/png"))
      errors.add(:logo, "image format is refused (png or jpeg only)")
    end
  end

end
