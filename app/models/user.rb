class User < ApplicationRecord
  rolify

  attr_accessor :login
  attr_accessor :role_name

  has_many :articles
  accepts_nested_attributes_for :articles
  accepts_nested_attributes_for :roles
  
  validates :username, presence: true, 
    uniqueness: { case_sensitive: false },
    format: { with: /\A[a-zA-Z0-9 _\.]*\z/ }

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable

  def self.find_first_by_auth_conditions(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      where(conditions.to_hash).where("lower(email) = :value OR lower(username) = :value",
                                      value: login.downcase).first
    else
      where(conditions.to_hash).first
    end
  end

  def self.role_name
    self.roles.first.name
  end

  def remove_only_role_relation(id)
    roles.delete(roles.where(id: id))
  end

end
