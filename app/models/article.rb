class Article < ApplicationRecord
  resourcify
  paginates_per 10
  acts_as_ordered_taggable_on :interests
  belongs_to :subject
  has_one_attached :logo
  belongs_to :user, inverse_of: :articles

  validates :user_id, presence: true
  validates :subject, presence: true
  validates :title, presence: true,
                    length: { maximum: 42, minimum: 3 },
                    uniqueness: { scope: :subject,
                                  message: "unique for each subject" }

  scope :published_only, -> { where "published_at IS NOT NULL" }
  scope :blacklisted_ids, -> (ids) { where.not(subject_id: ids) }

  def self.preferences_list(user)

  end

  def self.publishable_subjects_only
    unpublished_and_child = []
    Subject.where(published_at: nil).each { |a|
      unpublished_and_child += a.self_and_descendants.ids }
    blacklisted_ids(unpublished_and_child.uniq)
  end

end
