$(document).ready ->
  $("#new_article").on "ajax:success", (event) ->
    [data, status, xhr] = event.detail
    $(".notifications").html "<b>AJAX REQUEST SUCCESS</b>"
    return
  $("#new_article").on "ajax:error", (event) ->
    $(".notifications").append "<b>AJAX REQUEST FAILED</b>"
    return
  $('tr').not(':first').hover (-> 
    $(this).css "background", "#222222"
    return
  ), ->
    $(this).css "background", "#000000"
    return
  $(".edit_article").on "ajax:success", (event) ->
    console.log("get form submit for article edited")
    [data, status, xhr] = event.detail
    $('#test').html data
    return

