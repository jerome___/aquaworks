# js.coffee files to be referenced here for:
# lib/assets/javascripts | vendor/assets/javascript
#
# https://github.com/rails/sprockets#sprockets-directives
#
#= require jquery3
#= require jquery_ujs
#= require jquery.turbolinks
#= require turbolinks
#= require popper
#= require bootstrap
#= require activestorage
#= require cocoon
#= require trix
#= require jstree/jstree

$(document).ready ->
  namespace = (target, name, block) ->
    [target, name, block] = [(if typeof exports isnt 'undefined' then exports else window), arguments...] if arguments.length < 3
    top    = target
    target = target[item] or= {} for item in name.split '.'
    block target, top
  namespace 'notifications', (exports) ->
    exports.errors = (string_json, type) ->
      rails_json_ob = $.parseJSON(string_json)
      if rails_json_ob.hasOwnProperty('errors')
        $(".error").html ""
        string = '<%= j(render "layouts/error_notifications", ${type}: ${rails_json_ob}) %>'
        $(".notifications").fadeIn(500).html(string)
        for name in rails_json_ob.errors.each
          $(".article_${name}").append('<%= content_tag(:b, " ${rails_json_obj[name]} ", class: "error") %>')
      else
        $(".error").fadeOut(500).html("")
        $(".notifications").fadeOut(500).html("")
        $("#update_index_view").click()
