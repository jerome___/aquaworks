refreshTree = ->
  origin = window.location.origin
  url = origin + "/navigations/nav"
  $.getJSON url, (data) ->
    $("#container").jstree
      'core' :
        "check_callback" : true
        "multiple" : false
        "animation" : 250
        "data" :
          "url" : url
          "data_type" : "json"
      "theme" :
        "stripes" : true
        "icons" : true
      "types" :
        "#" :
          "valid_children" : "category"
        "category" :
          "icon" : "glyphicon glyphicon-hand-right"
          "valid_children" : "subject"
        "article" :
          "icon" : "glyphicon glyphicon-file"
        "subject" :
          "icon" : "glyphicon glyphicon-file"
        "default" :
          "icon" : "glyphicon glyphicon-sunglasses"
      "plugins" : [ "types" ]
    return
  .done ()->
    console.log "DONE (navigations|JS control|refreshTree function <= [JSON])"
  .fail ()->
    console.log "FAILED (navigations|JS control|refreshTree function <= [JSON])"
  return

getCategory = ->
  parents_grouped = $('#grouped_subjects').html()
  category = $('#subject_category_id :selected').text()
  options = $(parents_grouped).filter("optgroup[label='#{category}']").html()
  console.log("options: " + options)
  if options
    $('#subject_parent_id').html(options)
  else
    $('#subject_parent_id').html(parents_grouped)

window.refreshTree = refreshTree
window.getCategory = getCategory

$ ->
  $('#container').on 'select_node.jstree', (e, data)->
    console.log('div(id: "container")|jstree node <-- selected')
    if data.selected.length != 0
      regex = /(\d+).?/
      wid = data.instance.get_node(data.selected).id
      type = data.instance.get_node(data.selected).type
      match = wid.match(regex)
      id = match[1]
      url = "show_object/" + id + "/" + type
      console.log("url = " + url + " | obj: " + type + " | oid: " + id)
      $.get url, (data) ->
        $('#content_view').html(data)
    false
  return

$(document).ready ->
  console.log("page loaded with turbolink ready")
  origin = window.location.origin
  url = origin + "/navigations/nav"
  refreshTree(url)
  return
