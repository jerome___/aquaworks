$(document).ready ->
  $("#new_subject").on "ajax:success", (event) ->
    [data, status, xhr] = event.detail
    $(".notifications").html "<b>AJAX REQUEST SUCCESS</b>"
  $("#new_subject").on "ajax:error", (event) ->
    $(".notifications").append "<b>AJAX REQUEST FAILED</b>"
  $('tr').not(':first').hover (-> 
    $(this).css "background", "#222222"
    return
  ), ->
    $(this).css "background", "#000000"
    return

