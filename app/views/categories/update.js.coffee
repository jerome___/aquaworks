<% if @category.errors.any? %>
$(".error").html ""
$(".notifications").fadeIn(500).html('<%= j(render "layouts/error_notifications", article: @article) %>')
<% @category.errors.each do |name, msg| %>
$(".article_<%= name %>").append '<%= content_tag(:b, "#{msg} ", class: "error") %>'
<% end %>
<% else %>
$(".error").fadeOut(500).html("")
$(".notifications").fadeOut(500).html("")
row_category = "#category_" + <%= @category.id.to_s %> + "_row"
$(row_category).html '<%= j(render partial: "categories/show/index_table_row", locals: {category: @category, mode: "index" }) %>'
$(".modal").modal('hide')
refreshTree()
("#container").jstree("refresh")
<% end %>


