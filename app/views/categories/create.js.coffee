refreshTree()
("#container").jstree("refresh")
notifications.errors('<%= @category.to_json.html_safe %>', "category")

$("#update_index_view").click()
$('#table_index_body').append '<%= j(render partial: "categories/show_index_table_row" locals: { category: @category, mode: "index" }) %>'

