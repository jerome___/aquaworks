$(".dialog_action_container .modal-content").html '<%= j(render partial: "categories/form/form", locals: {name: @name, category: @category}) %>'
$(".dialog_action_container .modal-footer #publications").append '<%= j(render partial: "categories/form/form_links", locals: {name: @name, category: @category}) %>'
$(".modal").modal('show')
console.log("Category is editing [AJAX call] modal dialog")

