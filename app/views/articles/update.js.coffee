give_last = window.location.pathname.match(/.*\/(\w+)/)
give_index = give_last[1].match(/[^0-9]+/)
give_edit_id = give_last[1].match(/\d+/)

notifications.errors('<%= @article.to_json.html_safe %>', "article")

$("#update_index_view").click()
if give_index != null
  row_article = "#article_" + <%= @article.id.to_s %> + "_row"
  $(row_article).html '<%= j(render partial: "articles/show/index_table_row", locals: {article: @article, mode: "index" }) %>'
  console.log("Article id:" + <%= @article.id %> + "updated")
if give_edit_id != null
  $(".views").html '<%= j(render partial: "articles/show_view", article: @article) %>'
$(".modal").modal('hide')
