give_last = window.location.pathname.match(/.*\/(\w+)/)
give_index = give_last[1].match(/[^0-9]+/)
give_edit_id = give_last[1].match(/\d+/)

notifications.errors('<%= @article.to_json.html_safe %>', "article")

refreshTree()
("#container").jstree("refresh")

$("#update_index_view").click()
