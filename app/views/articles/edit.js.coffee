$(".dialog_action_container .modal-content").html '<%= j(render partial: "articles/form/form", locals: { article: @article, title: @title }) %>'
$(".dialog_action_container .modal-footer #publications").html '<%= j(render partial: "articles/form/form_links", locals: {title: @title, article: @article, mode: "index" }) %>'
$(".modal").modal('show')
console.log("Article is editing [AJAX call] modal dialog")
