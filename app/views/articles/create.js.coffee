refreshTree()
("#container").jstree("refresh")
notifications.errors('<%= @article.to_json.html_safe %>', "article")

$("#update_index_view").click()
$('#table_index_body').append '<%= j(render partial: "articles/show_index_table_row" locals: { article: @article, mode: "index" }) %>'
