json.extract! article, :id, :title, :description, :content
json.url article_url(article, format: :json)
