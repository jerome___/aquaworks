<%- @title = "Edit" %>
row_article = "#article_" + <%= @article.id.to_s %> + "_row"
console.log("article_row = " + row_article) 
$(".dialog_action_container .modal-footer #publications").html '<%= j(render partial: "articles/form/form_links", locals: { title: @title, article: @article }) %>'
$("#content_view").html '<%= j(render partial: "navigations/show/content_article", object: @article) %>'
$(row_article).html '<%= j(render partial: "articles/show/index_table_row", locals: {article: @article, mode: "index" }) %>'
