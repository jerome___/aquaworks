$(".dialog_action_container .modal-content").html '<%= j(render partial: "articles/form/form", locals: {title: @title, article: @article}) %>'
$(".dialog_action_container .modal-footer #publications").html '<%= j(render partial: "articles/form/form_links", locals: {article: @article, title: "new"}) %>'
console.log("New modal window (AJAX Mode) for new Article open")
