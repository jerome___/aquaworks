# After AJAX call from selected value of resource select box from new_role modal window, render content of resources_id select box with concerned content get from usersController.
# This file is called from usersController  method resources_for_type

$("#roles_resource_id").html '<%= j(render partial: "users/profil/admin/options_for_resources", locals: { resources: @resources }) %>'
