# populate dialog window modal container with selects boxes to be able to create a new user concerned role.
$(".dialog_action_container .modal-content").html '<%= j(render partial: "users/profil/admin/role_fields", locals: {user: @user }) %>'
$(".modal").modal('show') # show modal window
console.log("New user role form [AJAX call] modal dialog")

# when selection of resources type select box from the new_role modal window, call a function who POST selected value to usersController method called from Rails ROUTES in relation through AJAX call.
# The ROUTE will call method resources_for_type who will call back a javascript script with answer resources ids/values content to populate select box in relation.

populate_resource_name_from_type = ->
  resource_type = $("#roles_resource_type :selected").text()
  console.log("resource type: " + resource_type)
  $.ajax
    url: "populate_resources"
    type: "POST"
    dataType: "script"
    data: 
      resource_type_name: resource_type
  

$("#roles_resource_type").change populate_resource_name_from_type
