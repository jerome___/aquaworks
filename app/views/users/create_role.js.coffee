# called from JS render format usersController method update
# should render partial of concerned user bootstrap card box of edit tabs from admin's user edit page.

$('#user_<%= @user.id %>').html '<%= j(render partial: "users/profil/admin/role", locals: { user: @user, status: "edit" }) %>'
