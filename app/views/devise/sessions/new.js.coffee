$(".dialog_action_container .modal-content").html '<%= j(render partial: "devise/sessions/modal_new") %>'
$(".modal").modal("show")
$("#confirmation").hide()
$("#no_user_found").hide()
user_name=""

$ ->
  $("#send_email").on 'click', (ev) ->
    ev.preventDefault()
    console.log("Send email to " + user_name)
    $.ajax
      url: "users/send_confirmation"
      type: "POST"
      dataType: "script"
      data:
        username: user_name

exist_and_confirmed = (username) ->
  $.ajax
    url: "users/exist_confirmed"
    type: "POST"
    dataType: "json"
    data:
      username: username
    success: (data, status, xhr) -> 
      if data != undefined
        console.log("user account confirmed is " + data.confirmed + " STATUS = " + status)
        $("#no_user_found").hide()
        if data.confirmed 
          $("#connection").show()
          $("#confirmation").hide()
        else
          $("#connection").hide()
          $("#confirmation").show()
      else
        console.log("no username with this name")
        $("#no_user_found").show()
        $("#connection").hide()
        $("#confirmation").hide()
        

$('#user_username').on 'change', () -> 
  console.log("username is " + this.value )
  user_name = this.value
  exist_and_confirmed(user_name)
