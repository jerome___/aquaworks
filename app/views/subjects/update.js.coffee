give_last = window.location.pathname.match(/.*\/(\w+)/)
give_index = give_last[1].match(/[^0-9]+/)
give_edit_id = give_last[1].match(/\d+/)

notifications.errors('<%= @article.to_json.html_safe %>', "article")

$("#update_index_view").click()
if give_index != null
  row_subject = "#subject_" + <%= @subject.id.to_s %> + "_row"
  $(row_subjects).html '<%= j(render partial: "subjects/show/index_table_row", locals: {subject: @subject, mode: "index" }) %>'
  console.log("Subject id:" + <%= @subject.id %> + "updated")
if give_edit_id != null
  $(".views").html '<%= j(render partial: "subjects/show_view", subject: @subject) %>'
$(".modal").modal('hide')
