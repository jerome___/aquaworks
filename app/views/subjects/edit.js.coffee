$(".dialog_action_container .modal-content").html '<%= j(render partial: "subjects/form/form", locals: {title: @title, subject: @subject}) %>'
$(".dialog_action_container .modal-footer #publications").append '<%= j(render partial: "subjects/form/form_links", locals: {title: @title, subject: @subject}) %>'
$(".modal").modal('show')
console.log("Subject is editing [AJAX call] modal dialog")
