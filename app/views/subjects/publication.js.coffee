<%- @title = "Edit" %>
$(".dialog_action_container .modal-footer #publications").html '<%= j(render partial: "subjects/form/form_links", locals: {subject: @subject, title: @title}) %>'
$("#content_view").html '<%= j(render partial: "navigations/show/content_subject", object: @subject) %>'
console.log("Publication has been rendered")
