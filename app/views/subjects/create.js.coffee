refreshTree()
("#container").jstree("refresh")
notifications.errors('<%= @subject.to_json.html_safe %>', "subject")

$("#update_index_view").click()
$('#table_index_body').append '<%= j(render partial: "subjects/show_index_table_row" locals: { subject: @subject, mode: "index" }) %>'
