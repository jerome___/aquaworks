require 'test_helper'

class NavigationsControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get navigations_index_url
    assert_response :success
  end

  test "should get dated" do
    get navigations_dated_url
    assert_response :success
  end

  test "should get suscribed" do
    get navigations_suscribed_url
    assert_response :success
  end

  test "should get unread" do
    get navigations_unread_url
    assert_response :success
  end

  test "should get owner" do
    get navigations_owner_url
    assert_response :success
  end

end
