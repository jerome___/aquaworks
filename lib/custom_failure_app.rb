class CustomFailureApp < Devise::FailureApp
  def redirect
    store_location!
    message = warden.message || warden_options[:message]
    puts("Messages from Warden: #{message}")
    case message
    when  :timeout
      flash[:alert] = "Timing is running out..."
      redirect_to root_path
    when :unconfirmed     
      flash[:error] = "Your account is not confirmed"
      redirect_to root_path
    else 
      super
    end
  end
end
