# My Rails-5 web site

## The idea of this web site

1. To be able to have suffisant abstraction to be run as a full users-registred web site to create articles sorted by categories and subjects, and also "tags able".

1. To be able to get datas from a json API

1. To be safe

1. To be used with Rails-5 and Ruby-2.6

## Technologies used

- Full user's registration with Devise gem

- Precise and complexe roles access for users (default to suscribed, then Admin can manage users roles), i use Rolify and Cancancan

- Web site has many Categories

- Categories have many Subjects

- Articles belongs to Subject and Category (closure-tree gem used)

- forms to update/edit users and article and more (simple-form gem used and cocoon gem for nested forms)

- pagination of anything (kaminari gem used)

- tags for articles (acts-has-taggable gem used)

- user friendly text editor (trix-rails gem used)

- json api for search articles, categories, subjects (jsonapi-resources gem used)

- Nice views and responsive web site (Bootstrap-4 technology through bootstrap gem used)

- Databse data manged (Postgresql database with Rails-5 active-records)

- Upload/download images and files (for articles and iconify things) with mini-magick gem

- javascript used through jQuery and CoffeeScript libraries

- datas variables as secrets keys and logins things stay secret and defined as variable env through "figaro" gem (config/application.yml file is added to .gitignore to be hide)

## Links

- Look at this bitbucket git repo wiki page to get more infos on how to install and config this web site. Use it for yourself if you like it.

