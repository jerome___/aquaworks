# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path.
# Rails.application.config.assets.paths << Emoji.images_path
# Add Yarn node_modules folder to the asset load path.
Rails.application.config.assets.paths << Rails.root.join('node_modules')

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in the app/assets
# folder are already added.
# Rails.application.config.assets.precompile += %w( admin.js admin.css )
%w( welcome articles categories subjects navigations direct_upload 
    users users/registrations users/sessions ).each do |name|
	Rails.application.config.assets.precompile += ["#{name}.css", "#{name}.js"]
end
# get error because new ruby get back wrong null entry in list instead of nothing
%w( /\.(?:svg|eot|woff|ttf)\z/ ).each do |file|
  if file
    Rails.application.config.assets.precompile << file
  end
end
