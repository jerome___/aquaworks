Rails.application.routes.draw do

  root to: 'welcome#index'

  get 'navigations/index'
  get 'navigations/dated'
  get 'navigations/suscribed'
  get 'navigations/unread'
  get 'navigations/owner'
  get 'navigations/nav'
  get 'navigations/show_object/:oid/:obj', to: 'navigations#show_object', as: :show_object

  devise_for :users, 
              controllers: {sessions: 'users/sessions', 
                            registrations: 'users/registrations', 
                            confirmations: 'users/confirmations',
                            passwords: 'users/passwords' }
  resources :users do
    resources :roles, only: [:create, :destroy]
  end

  as :user do
    delete 'users/:id', to: 'users#destroy', as: :admin_destroy_user
    get 'users/:user_id/new_role', to: 'users#new_role', as: :admin_new_user_role
    delete 'users/:id/role/:role_id', to: 'users#destroy_role', as: :admin_destroy_user_role
    post 'users/populate_resources', to: "users#resources_for_type"
    post 'users/create_role/:id', to: "users#create_role", as: :admin_create_user_role
    post 'users/send_confirmation', to: 'users#send_confirmation_again', as: :send_user_confirmation
    post 'users/exist_confirmed', to: 'users#exist_confirmed', as: :user_exist_confirmed
  end

  resources :categories do
    resources :subjects do
      resources :articles
    end
  end

  resources :subjects do
    resources :articles
  end

  resources :articles, shallow: true

  get '/subjects/subject_parent/:parent_id/new', to: 'subjects#new', as: :new_subject_child
  get '/subjects/subject_parent/:parent_id/category/:category_id/new', to: 'subjects#new', as: :new_subject_child_category
  get "/articles/news/index", to: 'articles#fresh_index', 
                              as: "news_articles"
  post "/article/:id/:online",to: 'articles#publication', 
                              as: 'online_article'
  post "/subject/:id/:online",to: 'subjects#publication',
                              as: 'online_subject'
  get 'tags/:tag', to: 'articles#index', as: :tag

end
