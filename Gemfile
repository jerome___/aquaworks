source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '~> 2.6.0'
require 'rbconfig'
if RbConfig::CONFIG['target_os'] =~ /(?i-mx:bsd|dragonfly)/
  gem 'rb-kqueue', '>=0.2'
end


gem 'rails', '~> 5.2.0'
gem 'pg', '>= 0.18', '< 2.0'
gem 'puma', '~> 3.11'
gem 'sassc-rails', '~> 2.1.2'	# Use SASSc instead of traditionnal and slow one #gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'	  # Use Uglifier as compressor for JavaScript assets
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'mini_racer', platforms: :ruby

gem 'coffee-rails', '~> 4.2'	# Use CoffeeScript for .coffee assets and views
gem 'turbolinks', '~> 5'      # Turbolinks makes navigating your web application faster.
gem 'jbuilder', '~> 2.5'	    # Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'bcrypt', '~> 3.1.13'	    # Use ActiveModel has_secure_password
gem 'mini_magick', '~> 4.8'		# Use ActiveStorage variant
gem 'bootsnap', '>= 1.1.0', require: false	# Reduces boot times through caching; required in config/boot.rb
gem 'haml'			      # haml coding html pages ability
gem 'haml-rails'
gem 'bootstrap'			  # Twitter Bootstrap CSS framework
gem 'jquery-rails'		# add jQuery to Rails
gem 'jquery-turbolinks'
gem 'gon'             # share ruby variable to assets/view javascript code
gem 'figaro'          # Local ENV variable inside config/application.yml file (added to .gitignore)

gem 'closure_tree'            # add model objects tree ability with a separate table
gem 'simple_form'             # add facilities to write forms
gem 'cocoon'                  # add facilities to embed nested creation/views in forms/views.

gem 'devise'                  # add user authentication system login with email and managment users
gem 'cancancan', '~> 2.0'     # web site authorization management with users
gem 'rolify'                  # web site role management with users

gem 'kaminari'                # pagination on views
gem 'acts-as-taggable-on'     # tagging system

gem 'trix-rails', require: 'trix' # javascript simple editor window

#gem 'jsonapi-resources'

group :development, :test do
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]  # Call 'byebug' and get a debugger console pause
# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development
end

group :development do
  gem 'web-console', '>= 3.3.0'	# console on exception pages
  gem 'listen', '>= 3.0.5'#, '< 3.2'
  gem 'spring'					# keeping your application running in the background.
  #gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'awesome_print'
  #gem 'pry-rails'               # ADD binding.pry in a controller or somewhere and look at the result...
  #gem 'rails_db', '2.0.2'
  gem "better_errors"
  gem "binding_of_caller"
  #gem 'rails-erd'
  gem "rb-readline"
end

group :test do
  gem 'capybara', '>= 2.15', '< 4.0'	# Adds support for Capybara system testing and selenium driver
  gem 'selenium-webdriver'
end
